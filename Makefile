.PHONY: clean

CC = gcc
CFLAGS = -Wall -c -std=c11
OBJECTS = 

main: main.c
	$(CC) -o $@ $^

clean:
	-rm main *~
